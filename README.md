Install [Tailwind CSS standalone CLI](https://tailwindcss.com/blog/standalone-cli):

```sh
curl -sLO https://github.com/tailwindlabs/tailwindcss/releases/latest/download/tailwindcss-linux-arm64
chmod +x tailwindcss-linux-arm64
mv tailwindcss-linux-arm64 tailwindcss
```

Run it:

```sh
./tailwindcss -i style.css -o public/style.css --watch
```
