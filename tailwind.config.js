/* global module require */

const colors = require("tailwindcss/colors");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./public/index.html"],
  theme: {
    extend: {
      colors: {
        primary: colors.indigo,
        secondary: colors.yellow,
        error: colors.red,
        warning: colors.yellow,
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
